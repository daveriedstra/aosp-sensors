package com.daveriedstra.aospsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.os.VibrationEffect;
import android.os.Vibrator;

import org.puredata.core.PdBase;

public class MonitoredProximitySensor extends MonitoredSensor {
    private Vibrator vibrator;
    private boolean hasVibrator;
    public static final int MAX_VIB_DURATION = 1000 * 30;

    public MonitoredProximitySensor(Vibrator v, int _tvNameId, int _tvValueId) {
        super(Sensor.TYPE_PROXIMITY, _tvNameId, _tvValueId);
        vibrator = v;
        hasVibrator = v.hasVibrator();
    }

    @Override
    public void onSensorChanged(SensorEvent e) {
        if (MainActivity.UPDATE_TEXT)
            tvValue.setText(String.format("%s cm", e.values[0]));

        PdBase.sendFloat(PdResources.ProxSend, e.values[0]);

        if (hasVibrator) {
            if (e.values[0] < 1) {
                vibrator.vibrate(MAX_VIB_DURATION);
            } else {
                vibrator.cancel();
            }
        }
    }
}
