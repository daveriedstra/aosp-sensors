package com.daveriedstra.aospsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import org.puredata.core.PdBase;

public class MonitoredRotationSensor extends MonitoredSensor {
    public static float[] rotationMatrix = new float[9];
    public static boolean rotationMatrixOk;
    public MonitoredRotationSensor(int _tvNameId, int _tvValueId) {
        super(Sensor.TYPE_ROTATION_VECTOR, _tvNameId, _tvValueId);
    }

    @Override
    public void onSensorChanged(SensorEvent e) {
        if (MainActivity.UPDATE_TEXT) {
            String s = String.format("%s, %s, %s, %s, (%s rad)",
                    e.values[0], e.values[1], e.values[2], e.values[3], e.values[4]);
            tvValue.setText(s);
        }
    }
}
