package com.daveriedstra.aospsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

public class MonitoredLinearAccelSensor extends MonitoredSensor {
    public MonitoredLinearAccelSensor(int _tvNameId, int _tvValueId) {
        super(Sensor.TYPE_LINEAR_ACCELERATION, _tvNameId, _tvValueId);
    }

    // accel & linear accel & gravity: m/s^2, XYZ
    @Override
    public void onSensorChanged (SensorEvent e) {
        if (MainActivity.UPDATE_TEXT)
            tvValue.setText(String.format("X: %s, Y: %s, Z: %s m/s^2", e.values[0], e.values[1], e.values[2]));
    }
}
