package com.daveriedstra.aospsensors;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import org.puredata.android.io.AudioParameters;
import org.puredata.android.service.PdService;
import org.puredata.android.utils.PdUiDispatcher;
import org.puredata.core.PdBase;
import org.puredata.core.utils.IoUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private SensorManager mSensorManager;
    private ArrayList<MonitoredSensor> monitoredSensors = new ArrayList<>();
    public static final String TAG = "SensorsTest";
    public static boolean UPDATE_TEXT = false;
    BatteryTempReader battTempRdr = new BatteryTempReader();

    private PdService pdService = null;

    private final ServiceConnection pdConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            pdService = ((PdService.PdBinder)iBinder).getService();
            initPd();

            try {
                int sampleRate = AudioParameters.suggestSampleRate();
                int inputChannels = AudioParameters.suggestInputChannels();
                pdService.initAudio(sampleRate, inputChannels, 2, 8);
                pdService.startAudio();
            } catch (IOException e) {
                toast(e.toString());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            pdService.stopAudio();
        }
    };

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void initPd() {
        File patchFile = null;
        try {
            PdBase.setReceiver(new PdUiDispatcher());
            File dir = getFilesDir();
            IoUtils.extractZipResource(getResources().openRawResource(R.raw.patch), dir, true);
            patchFile = new File(dir, "main.pd");
            PdBase.openPatch(patchFile.getAbsolutePath());
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            finish();
        } finally {
            if (patchFile != null)
                patchFile.delete();
        }
    }

    // see https://developer.android.com/reference/android/hardware/SensorEvent#values
    // accel & linear accel & gravity: m/s^2, XYZ
    // magnetic field: XYZ in muT
    // light: SI lux units (lx; one lumen / m^2), [0]
    // prox: cm, [0]
    // gyro & corrected gyro: radians / sec, XYZ
    // rotation vector: 5 values; confusing, see documentation
    // orientation (legacy; suggested to use rotation vector instead)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        findViewById(R.id.linearLayout).setBackgroundColor(Color.parseColor(BuildConfig.BG_COLOR));
        UPDATE_TEXT = BuildConfig.DEBUG;

        // remove all textviews from root layout in release - we can't see them anyway
        if (!UPDATE_TEXT)
            ((ViewGroup) findViewById(R.id.linearLayout)).removeAllViews();

        if (UPDATE_TEXT) {
            MonitoredOrientationSensor.azimuthTv = findViewById(R.id.azimuthTv);
            MonitoredOrientationSensor.pitchTv = findViewById(R.id.pitchTv);
            MonitoredOrientationSensor.rollTv = findViewById(R.id.rollTv);
        }

        // init pd
        AudioParameters.init(this);
        bindService(new Intent(this, PdService.class), pdConnection, BIND_AUTO_CREATE);

        if (UPDATE_TEXT)
            battTempRdr.batteryTv = findViewById(R.id.batteryTempTv);

        // init sensors stuff
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        MonitoredProximitySensor proximity = new MonitoredProximitySensor(
            (Vibrator) getSystemService(Context.VIBRATOR_SERVICE),
            R.id.sensorName0,
            R.id.sensorValue0
        );
        initSensor(proximity);
        monitoredSensors.add(proximity);

        MonitoredLuxSensor light = new MonitoredLuxSensor(
            R.id.sensorName1,
            R.id.sensorValue1
        );
        initSensor(light);
        monitoredSensors.add(light);

        MonitoredMagneticSensor magnetic = new MonitoredMagneticSensor(
            R.id.sensorName2,
            R.id.sensorValue2
        );
        magnetic.directionTv = findViewById(R.id.magneticDirection);
        initSensor(magnetic);
        monitoredSensors.add(magnetic);

        MonitoredAccelSensor accel = new MonitoredAccelSensor(
                -1, -1);
        initSensor(accel);
        monitoredSensors.add(accel);

        MonitoredGyroSensor gyro = new MonitoredGyroSensor(
            R.id.sensorName3,
            R.id.sensorValue3
        );
        initSensor(gyro);
        monitoredSensors.add(gyro);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unbindService(pdConnection);
        } catch (IllegalArgumentException e) {
            pdService = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        battTempRdr.start();

        // enable fullscreen and dim screen
        WindowManager.LayoutParams p = getWindow().getAttributes();
        p.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        p.screenBrightness = 0.00001f;
        getWindow().setAttributes(p);

        for (MonitoredSensor m : monitoredSensors) {
            if (m.sensor != null) {
                mSensorManager.registerListener(m, m.sensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        battTempRdr.stop();

        // resume normal brightness (maybe not necessary)
        WindowManager.LayoutParams p = getWindow().getAttributes();
        p.screenBrightness = -1;
        getWindow().setAttributes(p);

        for (MonitoredSensor m : monitoredSensors) {
            mSensorManager.unregisterListener(m);
        }
    }

    @Override
    public void onBackPressed() { /* noop */ }

    private void initSensor(MonitoredSensor m) {
        m.sensor = mSensorManager.getDefaultSensor(m.sensorType);

        if (UPDATE_TEXT && m.tvValueId >= 0 && m.tvNameId >= 0) {
            m.tvName = findViewById(m.tvNameId);
            m.tvValue = findViewById(m.tvValueId);

            if (m.sensor == null) {
                m.tvValue.setText("no sensor found");
            } else {
                m.tvName.setText(m.sensor.getName());
            }
        }
    }

    private void toast(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
                toast.setText(TAG + ": " + text);
                toast.show();
            }
        });
    }
}
