package com.daveriedstra.aospsensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

public class MonitoredAccelSensor extends MonitoredSensor {
    // the data from the last sensor change
    public static float[] data = new float[3];

    public MonitoredAccelSensor(int _tvNameId, int _tvValueId) {
        super(Sensor.TYPE_ACCELEROMETER, _tvNameId, _tvValueId);
    }

    @Override
    public void onSensorChanged (SensorEvent e) {
        data = e.values.clone();
        MonitoredOrientationSensor.update();
    }
}
