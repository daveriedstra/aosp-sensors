package com.daveriedstra.aospsensors;

import android.hardware.SensorEvent;

import org.puredata.core.PdBase;

import java.util.ArrayList;

public abstract class AveragingMonitoredSensor extends MonitoredSensor {
    private ArrayList<Float> samples = new ArrayList<>();
    private float average = 0f;
    private float range = 0f;
    private static final float NUM_SAMPLES = 25f;
    protected float ACCEPTABLE_DEVIATION = 10f;
    private boolean wasDeviated = false;
    protected String pdDeviationAddress;
    protected String units = "";

    public AveragingMonitoredSensor(int _sensorType, int _tvNameId, int _tvValueId) {
        super(_sensorType, _tvNameId, _tvValueId);
    }

    @Override
    public void onSensorChanged(SensorEvent e) {
        float val = getValue(e);
        float deviation = Math.abs(val - average);

        if (MainActivity.UPDATE_TEXT)
            tvValue.setText(String.format("%.2f %s (%.2f away, %.2f avg, %.2f range)", val, units, deviation, average, range));

        reportDeviation(deviation);
        collectSample(val);
    }

    abstract float getValue(SensorEvent e);

    /**
     * report deviations while deviated.
     * when returning into acceptable threshold, report 0.
     */
    protected void reportDeviation(float deviation) {
        boolean isDeviated = deviation > ACCEPTABLE_DEVIATION;

        if (isDeviated)
            PdBase.sendFloat(pdDeviationAddress, deviation);
        else if (wasDeviated)
            PdBase.sendFloat(pdDeviationAddress, 0f);

        wasDeviated = isDeviated;
    }

    protected void collectSample(float f) {
        samples.add(f);

        if (samples.size() > NUM_SAMPLES) {
            samples.remove(0);
            getStats();
        }
    }

    /**
     * calculate average & range values for current sample set.
     * if more than half of the sample set are outside of the range
     * of the previous sample set, update average and range.
     */
    private void getStats() {
        float tally = 0f,
                min = 99999f,
                max = -99999f,
                numOutOfRange = 0f;

        for (float s : samples) {
            tally += s;
            if (s < min)
                min = s;
            if (s > max)
                max = s;
            if (Math.abs(s - average) > ACCEPTABLE_DEVIATION)
                numOutOfRange++;
        }

        if (numOutOfRange > NUM_SAMPLES / 2) {
            average = tally / NUM_SAMPLES;
            range = max - min;
        }
    }

}
